import React from "react";
import './sociallink.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faYoutube,
    faFacebook,
    faTwitter,
    faInstagram,
    faPinterest,
    faLinkedin
  } from "@fortawesome/free-brands-svg-icons";

export default function SocialLink(props) {
  
  return (
    
    <div class="social-container">
     
      <a style={{
        color:props.mycolor
      }} chref="https://www.facebook.com/learnbuildteach/"
        className="facebook social">
        <FontAwesomeIcon icon={faFacebook} size="1.8x" />
       </a>

      <a style={{
        color:props.mycolor
      }} href="https://www.youtube.com/c/jamesqquick"
  className="youtube social">
  <FontAwesomeIcon icon={faYoutube} size="1.8x" />
</a>

<a 
style={{
  color:props.mycolor
}} href="https://www.pinterest.com/c/jamesqquick"
  className="pinterest social">
  <FontAwesomeIcon icon={faPinterest} size="1.8x" />
</a>

<a 
style={{
  color:props.mycolor
}} href="https://www.Linkedin.com/c/jamesqquick"
  className="Linkedin social">
  <FontAwesomeIcon icon={faLinkedin} size="1.8x" />
</a>

<a style={{
        color:props.mycolor
      }} href="https://www.instagram.com/learnbuildteach"
  className="instagram social">
  <FontAwesomeIcon icon={faInstagram} size="1.8x" />
</a>

<a style={{
        color:props.mycolor
      }} href="https://www.twitter.com/jamesqquick" className="twitter social">
  <FontAwesomeIcon icon={faTwitter} size="1.8x" />
</a>
</div>
   
  );
  }