import React from "react"
import styled from "styled-components"

const TestimonialItem = (props) => {
 
    return (
        <Flex>
            <Para>{props.details}</Para>
            <SubContainer>
                <Round/>
                <Subtitle>ZIA
                    FT Packaging
                </Subtitle>
            </SubContainer>
        </Flex>
      )

}

export default TestimonialItem


const Subtitle = styled.h3`
  font-size: 18px;
  color: #fff;
  padding: 0 0 0 10px;
`

const Flex = styled.div`
  text-align:left;
  border-radius: 10px 50px 50px 50px;
  min-height: 200px;
  background-color: ${props => props.theme.color.lightgrey};
  @media (max-width: ${props => props.theme.screen.md}) {
    grid-template-columns: 1fr;
    grid-gap: 10px;
  }
`

const Round = styled.div`
  background-color: #000;
  width: 65px;
  height: 65px;
  border-radius: 50%;
  align-items: center;
  @media (max-width: ${props => props.theme.screen.md}) {
    grid-template-columns: 1fr;
    grid-gap: 10px;
  }
`

const SubContainer = styled.div`
  padding: 0 30px 30px 30px;
  display:flex;
  @media (max-width: ${props => props.theme.screen.md}) {
    grid-template-columns: 1fr;
    grid-gap: 10px;
  }
`


const Para = styled.p`
  color:#74889b;
  display: block;
  padding:30px;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
  @media (max-width: ${props => props.theme.screen.md}) {
    justify-self: center;
    
  }
`


