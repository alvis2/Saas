import React from "react"
import styled from "styled-components"

const FeatureContent = (props) => {
  
    return (

        <Container>
            <MainWrapper>
                <ImageWrapper>
                    <img style={{
                    height: "1em",
                    width: "1.25em",
                    }} src={require(`../../../images/product/${props.img}.svg`)} alt=""/>
                </ImageWrapper>
                <MainTitle>{props.title}</MainTitle>
            </MainWrapper>
            <Subtitle>{props.subtitle}</Subtitle>
        </Container>

       
      ) 
  
}

export default FeatureContent


const Subtitle = styled.p`
  font-size: 17px;
  color: ${props => props.theme.color.grey};
  
`

const MainTitle = styled.h3`
font-size: 18px;
    font-weight: 700;
    padding-bottom: 3px;
    margin-bottom: .5rem;
  color: ${props => props.theme.color.grey};
`

const ImageWrapper = styled.div`
  border: 4px solid ${props => props.theme.color.grey};
  min-width: 65px;
  height: 65px;
  border-radius: 20%;
  display:flex;
  justify-content:center;
  align-items:center;
  margin-right:10px;
  @media (max-width: ${props => props.theme.screen.md}) {
    justify-self: center;
  }
`
const MainWrapper = styled.div`
  display:flex;
  border-radius: 15px;
  @media (max-width: ${props => props.theme.screen.md}) {
    justify-self: center;
  }
`

const Container = styled.div`
  padding: 25px 15px;
  @media (max-width: ${props => props.theme.screen.md}) {
    justify-self: center;
    
  }
`