import React from "react"
import styled,{keyframes} from "styled-components"

const HeaderButton = () => {

    return (

          <AnchorContainer>
              <div style={{display:"flex",textAlign:"center",alignItems:"center",justifyContent:"center",padding:"25px 25px"}}>
                <Phone>
                <img style={{
                        width: "1em",
                        color: "#012970"
                        }} src={require(`../../../images/product/phone.svg`)} alt=""/>
                </Phone> 
                <Title>Lets's Talk</Title>
                <Arrow>
                <img style={{
                    width: ".875em",
                    height: "1em"
                    }} src={require(`../../../images/product/arrow-right.svg`)} alt=""/>
                </Arrow>
              </div>
          </AnchorContainer>
      ) 
}


export default HeaderButton


const AnchorContainer = styled.button`
    background-color: ${props => props.theme.color.blue};
    text-align:center;
    align-items:center;
    justify-content:center;
    border-radius: 30px;
    width: 285px;
    color: ${props => props.theme.color.themewhite};
`
const Arrow = styled.i`
    margin-left: 5px;
    font-size: 18px;
    transition: 0.3s;
`
const Phone = styled.div`
    background-color: ${props => props.theme.color.themewhite};
    border-radius: 50%;
    width: 35px;
    height: 35px;
    margin-right: 10px;
    align-items:center;
    justify-content:center;
    display:flex;
`
const Title = styled.span`
    font-family: 'Mulish', sans-serif;
    font-weight: 700;
    font-size: 21px;
    letter-spacing: 1px;
    text-align: center!important;
    justify-content:center;
`




