import React from "react"
import styled,{keyframes} from "styled-components"
import { graphql, useStaticQuery } from "gatsby"
import Img from "gatsby-image"
import SocialLink from "../common/sociallink/sociallink"
import HeaderButton from "../common/headerbutton/headerbutton"

const Header = () => {
  const data = useStaticQuery(graphql`
    query {
      file(sourceInstanceName: { eq: "product" }, name: { eq: "hero-img" }) {
        childImageSharp {
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid_tracedSVG
          }
        }
      }
    }
  `)

  const handleSubmit = event => {
    event.preventDefault()
  }

  return (
    <HeaderWrapper id="top">
      
        <Flex>
          <HeaderTextGroup>
            <Maintitle>
              Developing
              <br />
              Custom Built Web &
              <br />
              Mobile Applications
            </Maintitle>
            <Subtitle>
              Mobile App, Softwares,Artificial integlligence
            </Subtitle>
            <HeaderForm onSubmit={handleSubmit}>
              <HeaderButton/>
            </HeaderForm>
          </HeaderTextGroup>
          <ImageWrapper>
            <StyledImage fluid={data.file.childImageSharp.fluid} />
          </ImageWrapper>
        </Flex>
        <SocialLink mycolor={"#012970"}/>
    </HeaderWrapper>
  )
}

export default Header


const HeaderWrapper = styled.header`
  background-color: #f8f8f8;
  padding: 180px 20px;
  @media (max-width: ${props => props.theme.screen.md}) {
  }
`
const Maintitle = styled.h1`
  line-height: 1.2;
  font-size: 60px;
  font-weight: 700;
  font-family: 'Poppins', sans-serif;
`
const Subtitle = styled.h2`
  color: ${props => props.theme.color.grey};
  font-size: 20px;
  font-family: 'Poppins', sans-serif;
`
const HeaderTextGroup = styled.div`
  margin: 0;
  > div {
    width: 130%;
    margin-bottom: -4.5%;
    @media (max-width: ${props => props.theme.screen.md}) {
      margin: 0 16px;
    }
  }

  h1 {
    margin: 0 0 24px;
    color: "#ccc";
  }

  h2 {
    margin-bottom: 24px;
    ${props => props.theme.font_size.regular}
  }

  p {
    margin-bottom: 48px;
  }
`
const Flex = styled.div`
  display: grid;
  justify-content: space-between;
  align-content: center;
  grid-template-columns: 1fr 1fr;
  @media (max-width: ${props => props.theme.screen.md}) {
    grid-template-columns: 1fr;
    grid-gap: 64px;
  }
`
const HeaderForm = styled.form`
  display: flex;
  flex-direction: row;
  padding-bottom: 16px;
  @media (max-width: ${props => props.theme.screen.sm}) {
    flex-direction: column;
  }
`
const morph=keyframes`
  0% {padding-bottom:0%;}
  50% {padding-bottom:10%;}
  100% {padding-bottom:0%;}
`
const ImageWrapper = styled.div`
  justify-self: end;
  align-self: center;
  animation:${morph} 3s linear infinite;
  @media (max-width: ${props => props.theme.screen.md}) {
    justify-self: center;
  }
`
const StyledImage = styled(Img)`
  width: 550px;
  @media (max-width: ${props => props.theme.screen.md}) {
    
  }
  @media (max-width: ${props => props.theme.screen.sm}) {
    width: 300px;
    display: none;
  }
`

