import React from "react"
import styled from "styled-components"

import { Container, Section } from "../global"
import FeatureContent from "../common/featurecontent/featurecontent"
import ProjectImg from "../common/projectimg/projectimg"

const GetStarted = () => (
  <StyledSection>
    <GetStartedTitle>Our Features</GetStartedTitle>
    <GetStartedContainer>
      <FeatureContent img={"php1"} title={"PHP Based"} subtitle={"We develop websites and web applications using PHP’s Laravel framework."}/>
      <FeatureContent img={"cloud"} title={"Cloud Based"} subtitle={"Save time and money, quickly getting your development assets online."}/>
      <FeatureContent img={"cloud"} title={"UX Experience"} subtitle={"Enhance user satisfaction by improving usability, accessibility & efficiency."}/>
      <FeatureContent img={"user"} title={"User Friendly"} subtitle={"We deploy a user friendly interface with smooth interaction & accessibility."}/>
      <FeatureContent img={"tablet-and-laptop"} title={"Compatible"} subtitle={"Your website is coded to work regardless of the platform, browser or device."}/>
      <FeatureContent img={"file-alt"} title={"Content Management System"} subtitle={"Our powerful CMS enables you to publish your content, your way"}/>
      <FeatureContent img={"thumps-up"} title={"Quality Assurance Testing"} subtitle={"Our QA addresses all errors, bugs & compatibility, ensuring a speedy resolution."}/>
      <FeatureContent img={"headset"} title={"Full Support"} subtitle={"Uninterrupted maintenance support after website launch."}/>
    </GetStartedContainer>
    <GetStartedTitle>Our Featured Projects</GetStartedTitle>
    <ProjectContainer>
      <ProjectSub>
        <ProjectImg img={"1"}/>
        <TitleLeft>
          <MainTitle>Kayscience</MainTitle>
          <Subtitle>Let us help you with your needs in Web <br/>Application Development. Focus on your core <br/> business process while we strengthen your <br/>services.</Subtitle>
      </TitleLeft>
      </ProjectSub>
      <ProjectSub>
        <TitleRight>
          <MainTitle>Kayscience</MainTitle>
          <Subtitle>Let us help you with your needs in Web <br/>Application Development. Focus on your core <br/> business process while we strengthen your <br/>services.</Subtitle>
        </TitleRight>
        <ProjectImg img={"2"}/>
      </ProjectSub>
    </ProjectContainer>
    <ViewContainer>
      <ViewButton>View More</ViewButton>
    </ViewContainer>
  </StyledSection>
)

export default GetStarted

const StyledSection = styled(Section)`
  background-color: ${props => props.theme.color.background.light};
`
const GetStartedContainer = styled(Container)`
  display: grid;
  grid-template-columns:1fr 1fr 1fr 1fr;
  justify-content: top;
  align-items: left;
  flex-direction: column;
`
const GetStartedTitle = styled.h3`
  margin: 0 auto 32px;
  text-align: center;
`
const ProjectContainer = styled.div`
  align-items:center;
`
const ProjectSub = styled.div`
  display:flex;
  align-items:center;
  text-align:center;
  justify-content:center;
`
const ViewContainer = styled.div`
  display:flex;
  justify-content:center;
`

const ViewButton = styled.button`
  font-weight: 400;
  font-size: 1rem;
  color: white;
  letter-spacing: 1px;
  width:400px;
  height: 60px;
  display: block;
  margin-left: 8px;
  cursor: pointer;
  white-space: nowrap;
  background: ${props => props.theme.color.blue};
  border-radius: 30px;
  padding: 0px 40px;
  border-width: 0px;
  border-style: initial;
  border-color: initial;
  border-image: initial;
  outline: 0px;
  &:hover {
    box-shadow: rgba(110, 120, 152, 0.22) 0px 2px 10px 0px;
  }
  @media (max-width: ${props => props.theme.screen.md}) {
  }
  @media (max-width: ${props => props.theme.screen.sm}) {
    margin-left: 0;
  }
`

const Subtitle = styled.h4`
  font-size: 17px;
  color: ${props => props.theme.color.grey};
  
`
const MainTitle = styled.h3`
  font-size: 18px;
  font-weight: 700;
  color: ${props => props.theme.color.grey};
  
`
const TitleLeft = styled.div`
  text-align:left;
`

const TitleRight = styled.div`
  text-align:right;
`
