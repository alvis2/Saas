import React from "react"
import styled from "styled-components"


import { Section, Container } from "../global"

import ImageText from "../common/imagetext/imagetext"
import TechImg from "../common/techimg/techimg"

const Features = () => (

  <Section id="features">
    <StyledContainer>
      
      <PartnerGrid>
        <img style={{
                height: "70px",
                width: "160px",
                padding:"10px"
                }}src={require(`../../images/product/g-ccs-logo-w.png`)} alt=""/>
        <img style={{
                height: "70px",
                width: "160px",
                padding:"10px"
                }}src={require(`../../images/product/g-cloud-w.png`)} alt=""/>
        <img style={{
                height: "70px",
                width: "160px",
                padding:"10px"
                }}src={require(`../../images/product/google-partner.png`)} alt=""/>
      </PartnerGrid>
      <br/>
      <br/>
      <FeaturesGrid>

        <FeatureItem>
          <ImageText img={"computer"}/>
          <FeatureText>Full-Stack Web Development</FeatureText>
        </FeatureItem>

        <FeatureItem>
          <ImageText img={"software"}/>
          <FeatureText>Software Development</FeatureText>
        </FeatureItem>

        <FeatureItem>
          <ImageText img={"coding"}/>
          <FeatureText>Mobile App Development</FeatureText>
        </FeatureItem>

        <FeatureItem>
          <ImageText img={"chip"}/>
          <FeatureText>AI Development</FeatureText>
        </FeatureItem>
        
        <FeatureItem>
          <ImageText img={"ecom"}/>
          <FeatureText>E-commerce Website Design</FeatureText>
        </FeatureItem>
        
        <FeatureItem>
          <ImageText img={"shopify"}/>
          <FeatureText>Shopify Development</FeatureText>
        </FeatureItem>

      </FeaturesGrid>
      <SectionTitle>We deliver upon latest industry trends,<br/>
standards and implement quality-driven web solutions.</SectionTitle>
<Subtitle>Our solid domain experience, technical expertise and profound knowledge<br/>
help us develop websites tailored specifically to your business requirements.</Subtitle>
    </StyledContainer>
  
    <StyledContainer>
      <Flex>
        <div style={{
          padding:"60px 0px 0px 200px"
        }}>
          <SectionTitleLeft>Custom Web Development</SectionTitleLeft>
          <SubtitleLeft>
            Let us help you with your needs in Web <br/>
            Application Development. Focus on your core <br/>
            business process while we strengthen your<br/> 
            services.
          </SubtitleLeft>
        </div>
        
        <Linear>
          <LinearIn>
          <SectionTitle>
          We Love to Talk!
          <br/>
          Free Consultation
          <br/>
          with our Experts.
          </SectionTitle>
          <div style={{display:"grid", gridTemplateColumns:"1fr 1fr"}}>
            <div>
              <HeaderInput placeholder="Full Name" />
              <HeaderInput placeholder="E-mail" />
              <HeaderInput placeholder="Phone Number" />
            </div>
            <HeaderButton>-></HeaderButton>
          </div>
          
          </LinearIn>
          
          </Linear>
      </Flex>
    </StyledContainer>
    <br/>
    <br/>
    
      <TechGrid>
        <TechImg img={"html5"}/>
        <TechImg img={"css3"}/>
        <TechImg img={"js"}/>
        <TechImg img={"php"}/>
        <TechImg img={"jquery"}/>
      </TechGrid>
   
    
  </Section>
  
)

export default Features

const StyledContainer = styled(Container)`
text-align: center;
`

const SectionTitle = styled.h4`
  color: #dfe6e0;
  display: flex;
  justify-content: center;
  margin: 0 auto 40px;
  text-align: center;
  padding:60px 0px 0px 0px;
`
const SectionTitleLeft = styled.h4`
  color: #dfe6e0;
  text-align:left;
  
`
const SubtitleLeft = styled.h5`
  font-size: 16px;
  color: #788ca0;
  text-align:left;
`

const HeaderInput = styled.input`

background-color: #989bb3;
    border-radius: 10px;
    outline: none;
    border: none;
    height: 40px;
    margin-bottom: 5px;
    width: 220px;


`
const Subtitle = styled.h5`
  font-size: 16px;
  color: #788ca0;
  letter-spacing: 0px;
  margin-bottom: 60px;
  text-align: center;
`

const FeaturesGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr ;
  grid-column-gap: 40px;
  grid-row-gap: 35px;
  padding:10px 0;
  @media (max-width: ${props => props.theme.screen.sm}) {
    grid-template-columns: 1fr;
    padding: 0 64px;
  }
`

const TechGrid = styled.div`
  display: flex;
  justify-content: center;
  margin: 0 auto 40px;
  text-align: center;
  padding:20px 0;
  @media (max-width: ${props => props.theme.screen.sm}) {
    grid-template-columns: 1fr;
    padding: 0 0px;
  }
`

const HeaderButton = styled.button`
position: absolute;
color:#dfe6e0;
font-weight:bold;
font-size:25px;
background-color: #000;
border-radius: 10px;
outline: none;
border: none;
height: 130px;
width: 121px;
left: 266px;
background: rgb(16,155,216);
background: linear-gradient(
180deg
, rgba(16,155,216,1) 0%, rgba(35,44,100,1) 100%);
}
`


const PartnerGrid = styled.div`
  display: display: inline-block;;
  
  text-align:center;
  justify-content:center;
  @media (max-width: ${props => props.theme.screen.sm}) {
    grid-template-columns: 1fr;
    padding: 0 64px;
  }
`

const Linear = styled.div`
height: 400px;
width: 450px;
border-radius: 30px;
background: rgb(16,155,216);
background: linear-gradient(
0deg
, rgba(16,155,216,1) 0%, rgba(35,44,100,1) 100%);
  
`
const LinearIn = styled.div`
position: relative;
    height: 350px;
    width: 350px;
    border-radius: 23px;
    background-color: #1b1926;
    left: 30px;
    top: 75px;
    
`


const FeatureItem = styled.div`
  display: flex;
  justify-content: top;
  align-items: center;
  flex-direction: column;
`
const Flex = styled.div`
  display: grid;
  justify-content: space-between;
  align-content: center;
  grid-template-columns: 1fr 1fr;
  @media (max-width: ${props => props.theme.screen.md}) {
    grid-template-columns: 1fr;
    grid-gap: 64px;
  }
`
const FeatureText = styled.p`
  text-align: center;
  color:#dfe6e0;
  font-weight:bold;
`
