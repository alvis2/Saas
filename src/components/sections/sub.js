import React from "react"
import styled from "styled-components"
import { Section } from "../global"


const Sub = () => (

    <StyledSection>
            <SectionTitle>Have a project in mind?</SectionTitle>
            <Subtitle>Let's Create Something Amazing Together!?</Subtitle>
            <ViewButton>Get an Easy Quote</ViewButton>
    </StyledSection>
    
  
)

export default Sub

const  StyledSection= styled(Section)`
  background-color: ${props => props.theme.color.sub};
  text-align: center;
`
const ViewButton = styled.button`
  font-weight: 400;
  font-size: 1.4rem;
  font-weight:bold;
  color: ${props => props.theme.color.blue};
  width:400px;
  height: 60px;
  cursor: pointer;
  white-space: nowrap;
  background: ${props => props.theme.color.themewhite};
  border-radius: 30px;
  padding: 0px 40px;
  border-width: 0px;
  border-style: initial;
  border-color: initial;
  border-image: initial;
  outline: 0px;
  &:hover {
    box-shadow: rgba(110, 120, 152, 0.22) 0px 2px 10px 0px;
  }
  @media (max-width: ${props => props.theme.screen.md}) {
  }
  @media (max-width: ${props => props.theme.screen.sm}) {
    margin-left: 0;
  }
`
const SectionTitle = styled.h1`
    color: ${props => props.theme.color.themewhite};
`
const Subtitle = styled.h5`
  color: ${props => props.theme.color.themewhite};
`







