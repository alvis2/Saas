import React from "react"
import styled from "styled-components"
import { Section, Container } from "../global"
import TestimonialItem from "../common/testimonialitem/testimonialitem"

const Testimonials = () => (

  <Section id="features">
    <StyledContainer>
      <SectionTitle>Testimonials</SectionTitle>
      <TestimonialGrid>
        <TestimonialRow>
          <TestimonialItem details={"I have been working with Design Direct full time now for 3 years and to date, they have built and maintained 2 software products plus websites and new logos,as well as providing business support. They have been amazing the whole time, as they are an excellent team full of genuine, good people. We are now starting a new venture and I have 100% trust that they will produce another excellent product."}/>
          <TestimonialItem details={"I have been working with Design Direct full time now for 3 years and to date, they have built and maintained 2 software products plus websites and new logos,as well as providing business support."}/>
        </TestimonialRow>
        <TestimonialRow>
          <TestimonialItem details={"I have been working with Design Direct full time now for 3 years and to date, they have built"}/>
          <TestimonialItem details={"I have been working with Design Direct full time now for 3 years and to date, they have built and maintained 2 software products plus websites and new logos,as well as providing business support. They have been amazing the whole time, as they are an excellent team full of genuine, good people. We are now starting a new venture and I have 100% trust that they will produce another excellent product."}/>
        </TestimonialRow>
        <TestimonialRow>
          <TestimonialItem details={"I have been working with Design Direct full time now for 3 years and to date, they have built and maintained 2 software products plus websites and new logos,as well as providing business support. They have been amazing the whole time, as they are an excellent team full of genuine, good people. We are now starting a new venture and I have 100% trust that they will produce another excellent product."}/>
          <TestimonialItem details={"I have been working with Design Direct full time now for 3 years and to date, they have built and maintained 2 software products plus websites and new logos,as well as providing business support. They have been amazing the whole time"}/>
        </TestimonialRow>
      </TestimonialGrid>
      <div style={{
        textAlign:"center",
        justifyContent:"center",
        display:"flex"
      }}>
        <TestArrow>
          <img style={{
            width: ".875em",
            height: "1em"
                  }} src={require(`../../images/product/arrow-right.svg`)} alt=""/>
        </TestArrow>
      </div>
    </StyledContainer>
    
  </Section>
)

export default Testimonials

const StyledContainer = styled(Container)`
  text-align: center;

  max-width: 960px;
`
const SectionTitle = styled.h4`
  color: #dfe6e0;
  display: flex;
  justify-content: center;
  margin: 0 auto 40px;
  text-align: center;
  padding:60px 0px 0px 0px;
`
const TestArrow = styled.div`
  display:flex;
  background-color: #24242c;
  width: 65px;
  height: 65px;
  border-radius: 50%;
  align-items: center;
  text-align:center;
  justify-content:center;
  @media (max-width: ${props => props.theme.screen.sm}) {
  }
`
const TestimonialGrid = styled.div`
  --bs-gutter-x: 1.5rem;
  --bs-gutter-y: 0;
  display: flex;
  flex-wrap: wrap;
  margin-top: calc(var(--bs-gutter-y) * -1);
  margin-right: calc(var(--bs-gutter-x)/ -2);
  margin-left: calc(var(--bs-gutter-x)/ -2);
  @media (max-width: ${props => props.theme.screen.sm}) {
  }
`

const TestimonialRow = styled.div`
  display:block;
  flex: 0 0 auto;
  width: 33.3333333333%;
  padding-right: calc(var(--bs-gutter-x)/ 2);
    padding-left: calc(var(--bs-gutter-x)/ 2);
    margin-top: var(--bs-gutter-y);
}
  @media (max-width: ${props => props.theme.screen.sm}) {
  }
`







