import React from "react"
import styled from "styled-components"
import { Section } from "../global"
import Logo from "../common/logo/logo"
import SocialLink from "../common/sociallink/sociallink"
import HeaderButton from "../common/headerbutton/headerbutton"

const Footer = () => (

<StyledSection>
  <TopSection>
    <ContainerLeft>
      <Logo shadow={false}/>
      <SubTitle>
        Our industry experience and expertise allow <br/>
        us to consistently meet the digital needs of <br/>
        both startups and established businesses <br/>
        across London and the UK.
      </SubTitle>
    </ContainerLeft>
    <ContainerRight>
      <HeaderButton/>
      <p style={{fontSize:"18px",fontWeight:"700",lineHeight:"25px"}}>
        <strong style={{fontWeight:"bolder",fontSize:"25px"}}>Reach Us</strong><br/>
        Design Direct Web Solutions Ltd<br/>
        85 Trinity Avenue<br/>
        Enfield, EN1 1HT, UK
      </p> 
      
    </ContainerRight>
  </TopSection>
  <MiddleSection>
    <SubTitleLink>About</SubTitleLink>
    <SubTitleLink>Career</SubTitleLink>
    <SubTitleLink>Terms of Service</SubTitleLink>
    <SubTitleLink>Privacy Policy</SubTitleLink>
    <SubTitleLink>Cookie Policy</SubTitleLink>
    <SubTitleLink>Blog</SubTitleLink>
  </MiddleSection>
  <BottomSection>
    <div style={{textAlign:"left"}}>
      <div>
        © Copyright 2021 <strong>DesignDirectUK</strong>
      </div>
    </div>
    <div style={{textAlign:"center"}}>
      <div>
        <SocialLink mycolor={"#444444"}/>
      </div>
    </div>
    <div style={{textAlign:"right"}}>
      <div>
        <strong>info@designdirectuk.com</strong>
      </div>
    </div>
  </BottomSection>
 
  
</StyledSection>
)


const  StyledSection= styled(Section)`
  background-color: ${props => props.theme.color.themewhite};
  text-align: center;
  padding:30px 100px;
  color: ${props => props.theme.color.grey};
`
const TopSection = styled.div`
  display:grid;
  grid-template-columns: 1fr 1fr;
`
const MiddleSection = styled.div`
  display:flex;
  color: ${props => props.theme.color.grey};
  text-align:center;
  justify-content:center;
  align-items:center;
`
const BottomSection = styled.div`
  display:grid;
  grid-template-columns: 1fr 1fr 1fr;
`
const ContainerLeft = styled.div`
  text-align: left;
`
const ContainerRight = styled.div`
  text-align: right;
`
const SubTitle = styled.h4`
  font-size: 17px;
  color: ${props => props.theme.color.grey};
`
const SubTitleLink = styled.h4`
  padding:0px 20px;
  font-size: 17px;
  color: ${props => props.theme.color.grey};
`


export default Footer
